package au.com.blueskytel.datatransfer

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Main {
    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            SpringApplication.run(Main::class.java, *args)
        }
    }
}
