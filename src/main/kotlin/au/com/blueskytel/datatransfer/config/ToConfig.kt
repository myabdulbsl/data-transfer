package au.com.blueskytel.datatransfer.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.HashMap
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "toEntityManagerFactory", transactionManagerRef = "toTransactionManager", basePackages = ["au.com.blueskytel.datatransfer.repository.to"])
class ToConfig {

    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    fun toDataSourceProperties(): DataSourceProperties? {
        return DataSourceProperties()
    }

    @Primary
    @Bean
    fun toDataSource(@Qualifier("toDataSourceProperties") dataSourceProperties: DataSourceProperties): DataSource? {
        return dataSourceProperties.initializeDataSourceBuilder().build()
    }

    @Primary
    @Bean
    fun toEntityManagerFactory(@Qualifier("toDataSource") hubDataSource: DataSource?, builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean? {
        val properties: MutableMap<String, Any> = HashMap()
        properties["hibernate.hbm2ddl.auto"] = "update"
        return builder.dataSource(hubDataSource)
                .packages("au.com.blueskytel.datatransfer.model.common", "au.com.blueskytel.datatransfer.model.to")
                .properties(properties)
                .persistenceUnit("toDb")
                .build()
    }

    @Primary
    @Bean
    fun toTransactionManager(@Qualifier("toEntityManagerFactory") factory: EntityManagerFactory?): PlatformTransactionManager? {
        return JpaTransactionManager(factory!!)
    }

}