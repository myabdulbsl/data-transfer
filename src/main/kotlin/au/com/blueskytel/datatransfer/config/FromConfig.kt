package au.com.blueskytel.datatransfer.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.HashMap
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "fromEntityManagerFactory", transactionManagerRef = "fromTransactionManager", basePackages = ["au.com.blueskytel.datatransfer.repository.from"])
class FromConfig {

    @Bean
    @ConfigurationProperties(prefix = "from.datasource")
    fun fromDataSourceProperties(): DataSourceProperties? {
        return DataSourceProperties()
    }

    @Bean
    fun fromDataSource(@Qualifier("fromDataSourceProperties") dataSourceProperties: DataSourceProperties): DataSource? {
        return dataSourceProperties.initializeDataSourceBuilder().build()
    }

    @Bean(name = ["fromEntityManagerFactory"])
    fun fromEntityManagerFactory(@Qualifier("fromDataSource") fromDataSource: DataSource?, builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean? {
        val properties: MutableMap<String, Any> = HashMap()
        properties["hibernate.hbm2ddl.auto"] = "none"
        return builder.dataSource(fromDataSource)
                .packages("au.com.blueskytel.datatransfer.model.common")
                .properties(properties)
                .persistenceUnit("fromDb")
                .build()
    }

    @Bean
    fun fromTransactionManager(@Qualifier("fromEntityManagerFactory") factory: EntityManagerFactory?): PlatformTransactionManager? {
        return JpaTransactionManager(factory!!)
    }

}