package au.com.blueskytel.datatransfer.controller

import au.com.blueskytel.datatransfer.model.to.FileDumpModel
import au.com.blueskytel.datatransfer.repository.to.FileDumpRepository
import java.time.LocalDateTime

class FileDumpController(private val fileDumpRepository: FileDumpRepository) {

    fun updateCurrentTableId(integrationId: Long, integrationName: String, currentTableId: Long) {
        var fileDump = FileDumpModel(integrationId, integrationName, currentTableId)
        fileDumpRepository.findById(integrationId).ifPresent { x ->
            run {
                fileDump = x
                fileDump.updatedAt = LocalDateTime.now()
                fileDump.currentTableId = currentTableId
            }
        }
        fileDumpRepository.save(fileDump)
    }

}