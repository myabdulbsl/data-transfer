package au.com.blueskytel.datatransfer.controller

import au.com.blueskytel.datatransfer.repository.from.*
import au.com.blueskytel.datatransfer.repository.to.*
import au.com.blueskytel.datatransfer.service.IntegrationService
import au.com.blueskytel.datatransfer.service.db.*
import au.com.blueskytel.datatransfer.service.file_dump.AsteriskFDService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.Pollers
import org.springframework.integration.dsl.context.IntegrationFlowContext
import org.springframework.integration.endpoint.MethodInvokingMessageSource
import org.springframework.integration.handler.MethodInvokingMessageHandler
import org.springframework.stereotype.Controller
import java.util.function.Consumer

@Controller
class IntegrationController {

    @Autowired
    lateinit var integrationContext: IntegrationFlowContext

    @Autowired
    lateinit var integrationLogRepository: IntegrationLogRepository

    @Autowired
    lateinit var asteriskFromRepository: AsteriskFromRepository

    @Autowired
    lateinit var asteriskToRepository: AsteriskToRepository

    @Autowired
    lateinit var callTypeFromRepository: CallTypeFromRepository

    @Autowired
    lateinit var callTypeToRepository: CallTypeToRepository

    @Autowired
    lateinit var asteriskDeletedFromRepository: AsteriskDeletedFromRepository

    @Autowired
    lateinit var asteriskDeletedToRepository: AsteriskDeletedToRepository

    @Autowired
    lateinit var exportFromRepository: ExportFromRepository

    @Autowired
    lateinit var exportToRepository: ExportToRepository

    @Autowired
    lateinit var customerDataFromRepository: CustomerDataFromRepository

    @Autowired
    lateinit var customerDataToRepository: CustomerDataToRepository

    @Autowired
    lateinit var customerFromRepository: CustomerFromRepository

    @Autowired
    lateinit var customerToRepository: CustomerToRepository

    @Autowired
    lateinit var customerPlanFromRepository: CustomerPlanFromRepository

    @Autowired
    lateinit var customerPlanToRepository: CustomerPlanToRepository

    @Autowired
    lateinit var emersionCdrFromRepository: EmersionCdrFromRepository

    @Autowired
    lateinit var emersionCdrToRepository: EmersionCdrToRepository

    @Autowired
    lateinit var iddPrefixToRepository: IddPrefixToRepository

    @Autowired
    lateinit var iddPrefixFromRepository: IddPrefixFromRepository


    @Autowired
    lateinit var invoiceToRepository: InvoiceToRepository

    @Autowired
    lateinit var invoiceFromRepository: InvoiceFromRepository

    @Autowired
    lateinit var customerInvoiceToRepository: CustomerInvoiceToRepository

    @Autowired
    lateinit var customerInvoiceFromRepository: CustomerInvoiceFromRepository


    @Autowired
    lateinit var planFromRepository: PlanFromRepository

    @Autowired
    lateinit var planToRepository: PlanToRepository

    @Autowired
    lateinit var planPrefixFromRepository: PlanPrefixFromRepository

    @Autowired
    lateinit var planPrefixToRepository: PlanPrefixToRepository

    @Autowired
    lateinit var prefixFromRepository: PrefixFromRepository

    @Autowired
    lateinit var prefixToRepository: PrefixToRepository

    @Autowired
    lateinit var fileDumpRepository: FileDumpRepository

    @Bean
    fun integration() {
        print("Spooling up integrations\n")

        var integrationList = mutableListOf<IntegrationService>()
        val asteriskService = AsteriskService()
        asteriskService.asteriskFromRepository = asteriskFromRepository
        asteriskService.asteriskToRepository = asteriskToRepository
        asteriskService.integrationLogRepository = integrationLogRepository
        integrationList.add(asteriskService)
        val callTypeService = CallTypeService()
        callTypeService.callTypeFromRepository = callTypeFromRepository
        callTypeService.callTypeToRepository = callTypeToRepository
        callTypeService.integrationLogRepository = integrationLogRepository
        integrationList.add(callTypeService)
        val asteriskDeletedService = AsteriskDeletedService()
        asteriskDeletedService.asteriskDeletedFromRepository = asteriskDeletedFromRepository
        asteriskDeletedService.asteriskDeletedToRepository = asteriskDeletedToRepository
        asteriskDeletedService.integrationLogRepository = integrationLogRepository
        integrationList.add(asteriskDeletedService)
        val exportService = ExportService()
        exportService.exportToRepository = exportToRepository
        exportService.exportFromRepository = exportFromRepository
        exportService.integrationLogRepository = integrationLogRepository
        integrationList.add(exportService)
        val customerDataService = CustomerDataService()
        customerDataService.customerDataToRepository = customerDataToRepository
        customerDataService.customerDataFromRepository = customerDataFromRepository
        customerDataService.integrationLogRepository = integrationLogRepository
        integrationList.add(customerDataService)
        val customerService = CustomerService()
        customerService.customerToRepository = customerToRepository
        customerService.customerFromRepository = customerFromRepository
        customerService.integrationLogRepository = integrationLogRepository
        integrationList.add(customerService)
        val customerPlanService = CustomerPlanService()
        customerPlanService.customerPlanToRepository = customerPlanToRepository
        customerPlanService.customerPlanFromRepository = customerPlanFromRepository
        customerPlanService.integrationLogRepository = integrationLogRepository
        integrationList.add(customerPlanService)
        val emersionCdrService = EmersionCdrService()
        emersionCdrService.emersionCdrFromRepository = emersionCdrFromRepository
        emersionCdrService.emersionCdrToRepository = emersionCdrToRepository
        emersionCdrService.integrationLogRepository = integrationLogRepository
        integrationList.add(emersionCdrService)
        val iddPrefixService = IddPrefixService()
        iddPrefixService.iddPrefixFromRepository = iddPrefixFromRepository
        iddPrefixService.iddPrefixToRepository = iddPrefixToRepository
        iddPrefixService.integrationLogRepository = integrationLogRepository
        integrationList.add(iddPrefixService)
        val invoiceService = InvoiceService()
        invoiceService.invoiceFromRepository = invoiceFromRepository
        invoiceService.invoiceToRepository = invoiceToRepository
        invoiceService.integrationLogRepository = integrationLogRepository
        integrationList.add(invoiceService)
        val customerInvoiceService = CustomerInvoiceService()
        customerInvoiceService.customerInvoiceFromRepository = customerInvoiceFromRepository
        customerInvoiceService.customerInvoiceToRepository = customerInvoiceToRepository
        customerInvoiceService.integrationLogRepository = integrationLogRepository
        integrationList.add(customerInvoiceService)
        val planService = PlanService()
        planService.planFromRepository = planFromRepository
        planService.planToRepository = planToRepository
        planService.integrationLogRepository = integrationLogRepository
        integrationList.add(planService)
        val planPrefixService = PlanPrefixService()
        planPrefixService.planPrefixFromRepository = planPrefixFromRepository
        planPrefixService.planPrefixToRepository = planPrefixToRepository
        planPrefixService.integrationLogRepository = integrationLogRepository
        integrationList.add(planPrefixService)
        val prefixService = PrefixService()
        prefixService.prefixFromRepository = prefixFromRepository
        prefixService.prefixToRepository = prefixToRepository
        prefixService.integrationLogRepository = integrationLogRepository
        integrationList.add(prefixService)
        val asteriskFDService = AsteriskFDService()
        asteriskFDService.asteriskToRepository = asteriskToRepository
        asteriskFDService.integrationLogRepository = integrationLogRepository
        asteriskFDService.fileDumpRepository = fileDumpRepository
        integrationList.add(asteriskFDService)

        //dynamically spin up integrations from database configurations
        for (integration in integrationList) {

            //set the message source
            val ms = MethodInvokingMessageSource()
            ms.setObject(integration)
            ms.setMethodName("getSource")

            //set the message handler
            val h = MethodInvokingMessageHandler(integration, "getHandler")

            val pollRate: Long = if (integration.integrationType == 0) 60000 else 3600000
//            val pollRate: Long = if (integration.integrationType == 0) 15000 else 60000
            //create the integration flow
            val integrationFlow = IntegrationFlows.from(ms, Consumer { c -> c.poller(Pollers.fixedRate(pollRate)) })
                    .handle(h)
                    .get()

            //register the new integration flow
            integrationContext.registration(integrationFlow).register()
        }

    }

}