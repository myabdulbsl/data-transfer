package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.PlanPrefixModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PlanPrefixFromRepository : JpaRepository<PlanPrefixModel, Long> {
}