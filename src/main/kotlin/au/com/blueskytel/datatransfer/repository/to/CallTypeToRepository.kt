package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.CallTypeModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CallTypeToRepository : JpaRepository<CallTypeModel, Long> {
    fun findTop1ByOrderByCallTypeIDDesc(): CallTypeModel
}