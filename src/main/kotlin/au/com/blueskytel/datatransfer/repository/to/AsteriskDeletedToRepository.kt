package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.AsteriskDeletedModel
import au.com.blueskytel.datatransfer.model.common.AsteriskModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AsteriskDeletedToRepository : JpaRepository<AsteriskDeletedModel, Long> {
    fun findTop1ByOrderByIdDesc(): AsteriskDeletedModel
}