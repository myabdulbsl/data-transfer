package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.to.FileDumpModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FileDumpRepository : JpaRepository<FileDumpModel, Long> {
}