package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.CallTypeModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CallTypeFromRepository : JpaRepository<CallTypeModel, Long> {
    fun findTop1000ByCallTypeIDGreaterThanOrderByCallTypeID(id: Long): List<CallTypeModel>
}