package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.AsteriskModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AsteriskToRepository : JpaRepository<AsteriskModel, Long> {
    fun findTop1ByOrderByIdDesc(): AsteriskModel
    fun findByIdGreaterThanOrderById(id: Long): List<AsteriskModel>
}