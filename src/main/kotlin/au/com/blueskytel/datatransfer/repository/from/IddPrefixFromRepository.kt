package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.CallTypeModel
import au.com.blueskytel.datatransfer.model.common.ExportModel
import au.com.blueskytel.datatransfer.model.common.IddPrefixModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface IddPrefixFromRepository : JpaRepository<IddPrefixModel, Long> {
    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<IddPrefixModel>
}