package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.PlanModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PlanFromRepository : JpaRepository<PlanModel, Long> {
    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<PlanModel>
}