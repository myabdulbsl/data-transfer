package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.CallTypeModel
import au.com.blueskytel.datatransfer.model.common.ExportModel
import au.com.blueskytel.datatransfer.model.common.InvoiceModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InvoiceToRepository : JpaRepository<InvoiceModel, Long> {
    fun findTop1ByOrderByIdDesc(): InvoiceModel
}