package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PlanPrefixToRepository : JpaRepository<PlanPrefixModel, Long> {
}