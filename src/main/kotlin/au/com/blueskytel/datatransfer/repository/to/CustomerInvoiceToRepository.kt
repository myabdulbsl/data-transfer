package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.CustomerInvoiceModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CustomerInvoiceToRepository : JpaRepository<CustomerInvoiceModel, Long> {
    fun findTop1ByOrderByIdDesc(): CustomerInvoiceModel
}