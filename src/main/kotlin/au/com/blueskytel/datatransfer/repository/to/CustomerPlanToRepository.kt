package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.CustomerPlanModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CustomerPlanToRepository : JpaRepository<CustomerPlanModel, Long> {
    fun findTop1ByOrderByIdDesc(): CustomerPlanModel
}