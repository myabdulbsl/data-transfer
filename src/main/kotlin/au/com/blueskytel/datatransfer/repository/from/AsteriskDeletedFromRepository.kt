package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.AsteriskDeletedModel
import au.com.blueskytel.datatransfer.model.common.AsteriskModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AsteriskDeletedFromRepository : JpaRepository<AsteriskDeletedModel, Long> {

    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<AsteriskDeletedModel>

}