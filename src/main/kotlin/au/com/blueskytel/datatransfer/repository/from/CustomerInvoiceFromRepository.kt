package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.CustomerInvoiceModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CustomerInvoiceFromRepository : JpaRepository<CustomerInvoiceModel, Long> {
    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<CustomerInvoiceModel>
}