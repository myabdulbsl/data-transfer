package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.PrefixModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PrefixFromRepository : JpaRepository<PrefixModel, Long> {
    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<PrefixModel>
}