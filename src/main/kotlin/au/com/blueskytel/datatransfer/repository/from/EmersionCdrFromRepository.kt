package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.EmersionCdrModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmersionCdrFromRepository : JpaRepository<EmersionCdrModel, Long> {
    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<EmersionCdrModel>
}