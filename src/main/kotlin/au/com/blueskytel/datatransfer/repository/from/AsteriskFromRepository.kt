package au.com.blueskytel.datatransfer.repository.from

import au.com.blueskytel.datatransfer.model.common.AsteriskModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AsteriskFromRepository : JpaRepository<AsteriskModel, Long> {

    fun findTop1000ByIdGreaterThanOrderById(id: Long): List<AsteriskModel>

}