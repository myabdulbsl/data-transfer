package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.EmersionCdrModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmersionCdrToRepository : JpaRepository<EmersionCdrModel, Long> {
    fun findTop1ByOrderByIdDesc(): EmersionCdrModel
}