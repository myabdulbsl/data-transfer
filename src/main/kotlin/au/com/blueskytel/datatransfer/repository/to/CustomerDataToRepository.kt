package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.CallTypeModel
import au.com.blueskytel.datatransfer.model.common.CustomerDataModel
import au.com.blueskytel.datatransfer.model.common.ExportModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CustomerDataToRepository : JpaRepository<CustomerDataModel, Long> {
    fun findTop1ByOrderByIdDesc(): CustomerDataModel
}