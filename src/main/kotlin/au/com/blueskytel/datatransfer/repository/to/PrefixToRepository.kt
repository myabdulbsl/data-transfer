package au.com.blueskytel.datatransfer.repository.to

import au.com.blueskytel.datatransfer.model.common.PrefixModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PrefixToRepository : JpaRepository<PrefixModel, Long> {
    fun findTop1ByOrderByIdDesc(): PrefixModel
}