package au.com.blueskytel.datatransfer.service

import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository

interface IntegrationService {
    var integrationId: Int
    var integrationType: Int
    var integrationName: String
    var integrationLogRepository: IntegrationLogRepository
    fun getSource(): Any?
    fun getHandler(data: Any)
}