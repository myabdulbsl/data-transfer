package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.AsteriskModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.AsteriskFromRepository
import au.com.blueskytel.datatransfer.repository.to.AsteriskToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class AsteriskService : IntegrationService {

    override var integrationId = 1
    override var integrationType = 0
    override var integrationName = "Asterisk"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var asteriskFromRepository: AsteriskFromRepository
    lateinit var asteriskToRepository: AsteriskToRepository

    override fun getSource(): Any? {

        var asteriskFromList: List<AsteriskModel> = listOf()
        try {
            val asteriskTo = if (asteriskToRepository.count() > 0)
                asteriskToRepository.findTop1ByOrderByIdDesc()
            else
                null
            asteriskFromList = if (asteriskTo != null)
                asteriskTo.id?.let { asteriskFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<AsteriskModel>
            else
                asteriskFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (asteriskFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Source: Retrieved ${asteriskFromList.size} rows"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Source: ${e.message}", isError = true))
        }

        return asteriskFromList
    }

    override fun getHandler(data: Any) {

        try {
            val asteriskToList = data as List<AsteriskModel>
            if (asteriskToList != null && asteriskToList.isNotEmpty()) {
                asteriskToRepository.saveAll(asteriskToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: Pushed ${asteriskToList.size} rows"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: ${e.message}", isError = true))
        }

    }
}