package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.AsteriskDeletedModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.AsteriskDeletedFromRepository
import au.com.blueskytel.datatransfer.repository.to.AsteriskDeletedToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class AsteriskDeletedService : IntegrationService {

    override var integrationId = 2
    override var integrationType = 0
    override var integrationName = "AsteriskDeleted"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var asteriskDeletedFromRepository: AsteriskDeletedFromRepository
    lateinit var asteriskDeletedToRepository: AsteriskDeletedToRepository

    override fun getSource(): Any? {

        var asteriskDeletedFromList: List<AsteriskDeletedModel> = listOf()
        try {
            val asteriskDeletedTo = if (asteriskDeletedToRepository.count() > 0)
                asteriskDeletedToRepository.findTop1ByOrderByIdDesc()
            else
                null
            asteriskDeletedFromList = if (asteriskDeletedTo != null)
                asteriskDeletedTo.id?.let { asteriskDeletedFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<AsteriskDeletedModel>
            else
                asteriskDeletedFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (asteriskDeletedFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${asteriskDeletedFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return asteriskDeletedFromList
    }

    override fun getHandler(data: Any) {

        try {
            val asteriskDeletedToList = data as List<AsteriskDeletedModel>
            if (asteriskDeletedToList != null && asteriskDeletedToList.isNotEmpty()) {
                asteriskDeletedToRepository.saveAll(asteriskDeletedToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${asteriskDeletedToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}