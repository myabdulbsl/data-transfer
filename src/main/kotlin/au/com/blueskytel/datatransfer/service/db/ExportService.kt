package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.ExportModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.ExportFromRepository
import au.com.blueskytel.datatransfer.repository.to.ExportToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class ExportService : IntegrationService {

    override var integrationId: Int = 4
    override var integrationType = 0
    override var integrationName = "Export"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var exportToRepository: ExportToRepository
    lateinit var exportFromRepository: ExportFromRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<ExportModel> = listOf()
        try {
            val exportTo = if (exportToRepository.count() > 0)
                exportToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (exportTo != null)
                exportTo.id?.let { exportFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<ExportModel>
            else
                exportFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {
        try {
            val exportToList = data as List<ExportModel>
            if (exportToList != null && exportToList.isNotEmpty()) {
                exportToRepository.saveAll(exportToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${exportToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}