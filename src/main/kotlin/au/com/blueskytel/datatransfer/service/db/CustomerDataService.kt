package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.CustomerDataModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.CustomerDataFromRepository
import au.com.blueskytel.datatransfer.repository.to.CustomerDataToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class CustomerDataService : IntegrationService {

    override var integrationId: Int = 5
    override var integrationType = 0
    override var integrationName = "CustomerData"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var customerDataFromRepository: CustomerDataFromRepository
    lateinit var customerDataToRepository: CustomerDataToRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<CustomerDataModel> = listOf()
        try {
            val customerDataTo = if (customerDataToRepository.count() > 0)
                customerDataToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (customerDataTo != null)
                customerDataTo.id?.let { customerDataFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<CustomerDataModel>
            else
                customerDataFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val customerDataToList = data as List<CustomerDataModel>
            if (customerDataToList != null && customerDataToList.isNotEmpty()) {
                customerDataToRepository.saveAll(customerDataToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${customerDataToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}