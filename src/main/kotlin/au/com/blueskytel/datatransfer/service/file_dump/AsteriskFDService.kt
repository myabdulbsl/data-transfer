package au.com.blueskytel.datatransfer.service.file_dump

import au.com.blueskytel.datatransfer.controller.FileDumpController
import au.com.blueskytel.datatransfer.model.common.AsteriskModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.to.AsteriskToRepository
import au.com.blueskytel.datatransfer.repository.to.FileDumpRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import com.jcraft.jsch.*
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


@Service
class AsteriskFDService : IntegrationService {

    override var integrationId = 15
    override var integrationType = 1
    override var integrationName = "Asterisk-FD"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var asteriskToRepository: AsteriskToRepository
    lateinit var fileDumpRepository: FileDumpRepository

    override fun getSource(): Any? {

        var asteriskToList: List<AsteriskModel>? = listOf()
        try {
            var currentTableId: Long = 0
            fileDumpRepository.findById(integrationId.toLong()).ifPresent { x -> currentTableId = x.currentTableId!! }
            asteriskToList = asteriskToRepository.findByIdGreaterThanOrderById(currentTableId)
            if (asteriskToList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Source: Retrieved ${asteriskToList.size} rows"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Source: ${e.message}", isError = true))
        }

        return asteriskToList
    }

    override fun getHandler(data: Any) {

        try {
            val date = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("yyyyMMddHH")
            val dateStr = date.format(formatter)
            val csvFile = "CDR_${dateStr}00.csv"

            val asteriskToList = data as List<AsteriskModel>
            if (asteriskToList != null && asteriskToList.isNotEmpty()) {
                writeToCsv(asteriskToList, csvFile)
                if (uploadFile(csvFile))
                    asteriskToList.last().id?.let { FileDumpController(fileDumpRepository).updateCurrentTableId(integrationId.toLong(), integrationName, it) }
                deleteFile(csvFile)
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: ${e.message}", isError = true))
        }

    }

    fun writeToCsv(list: List<AsteriskModel>, file: String) {
        var fileWriter: FileWriter? = null

        try {
            val dateFormatter = DateTimeFormatter.ofPattern("d/MM/yyyy h:mm:ss a")
            fileWriter = FileWriter(file)

            fileWriter.append("id,calldate,clid,src,dst,diallingnumber,diallednumber,dcontext,channel,dstchannel,lastapp,lastdata,duration,billsec,disposition,amaflags,accountcode,uniqueid,userfield,duplicate,duplicatetoaccountcode,calldirection,callforwardnumber,incomingcharged")
            fileWriter.append('\n')

            for (item in list) {
                fileWriter.append("${replaceIfNull(item.id)},")
                fileWriter.append("${replaceIfNull(item.callDate?.format(dateFormatter))},")
                fileWriter.append("${replaceIfNull(item.clId)},")
                fileWriter.append("${replaceIfNull(item.src)},")
                fileWriter.append("${replaceIfNull(item.dst)},")
                fileWriter.append("${replaceIfNull(item.diallingNumber)},")
                fileWriter.append("${replaceIfNull(item.dialledNumber)},")
                fileWriter.append("${replaceIfNull(item.dconText)},")
                fileWriter.append("${replaceIfNull(item.channel)},")
                fileWriter.append("${replaceIfNull(item.dstChannel)},")
                fileWriter.append("${replaceIfNull(item.lastApp)},")
                fileWriter.append("${replaceIfNull(item.lastData)},")
                fileWriter.append("${replaceIfNull(item.duration)},")
                fileWriter.append("${replaceIfNull(item.billsec)},")
                fileWriter.append("${replaceIfNull(item.disposition)},")
                fileWriter.append("${replaceIfNull(item.amaFlags)},")
                fileWriter.append("${replaceIfNull(item.accountCode)},")
                fileWriter.append("${replaceIfNull(item.uniqueId)},")
                fileWriter.append("${replaceIfNull(item.userField)},")
                fileWriter.append("${replaceIfNull(item.duplicate)},")
                fileWriter.append("${replaceIfNull(item.duplicateToAccountCode)},")
                fileWriter.append("${replaceIfNull(item.callDirection)},")
                fileWriter.append("${replaceIfNull(item.callForwardNumber)},")
                fileWriter.append("${replaceIfNull(item.incomingCharged)}")
                fileWriter.append('\n')
            }
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: Generated CSV file"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: ${e.message}", isError = true))
        } finally {
            try {
                fileWriter!!.flush()
                fileWriter.close()
            } catch (e: IOException) {
                e.printStackTrace()
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: ${e.message}", isError = true))
            }
        }
    }

    @Throws(JSchException::class)
    private fun setupJsch(): ChannelSftp? {
        val remoteHost = "10.2.7.199"
        val username = "cdr-vm"
        val password = "\$t0pn0w!!!"
        val jsch = JSch()
        val jschSession: Session = jsch.getSession(username, remoteHost)
        jschSession.setPassword(password)
        val config = Properties()
        config["StrictHostKeyChecking"] = "no"
        jschSession.setConfig(config)
        jschSession.connect()
        return jschSession.openChannel("sftp") as ChannelSftp?
    }

    @Throws(JSchException::class, SftpException::class)
    fun uploadFile(file: String): Boolean {
        return try {
            val channelSftp = setupJsch()
            channelSftp!!.connect()
            val remoteDir = ""
            channelSftp.put(file, remoteDir + file)
            channelSftp.exit()
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: Dumped CSV file"))
            true
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Handler: ${e.message}", isError = true))
            false
        }
    }

    fun deleteFile(file: String) {
        val csvFile = File(file)
        if (csvFile.exists())
            csvFile.delete()
    }

    fun replaceIfNull(data: Any?): String {
        return data?.toString() ?: ""
    }

}