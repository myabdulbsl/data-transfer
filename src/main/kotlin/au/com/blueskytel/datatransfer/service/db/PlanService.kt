package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.PlanModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.PlanFromRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.repository.to.PlanToRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class PlanService : IntegrationService {

    override var integrationId: Int = 12
    override var integrationType = 0
    override var integrationName = "Plan"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var planFromRepository: PlanFromRepository
    lateinit var planToRepository: PlanToRepository

    override fun getSource(): Any? {

        var planFromList: List<PlanModel> = listOf()
        try {
            val planTo = if (planToRepository.count() > 0)
                planToRepository.findTop1ByOrderByIdDesc()
            else
                null
            planFromList = if (planTo != null)
                planTo.id?.let { planFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<PlanModel>
            else
                planFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (planFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${planFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }
        return planFromList
    }

    override fun getHandler(data: Any) {

        try {
            val planToList = data as List<PlanModel>
            if (planToList != null && planToList.isNotEmpty()) {
                planToRepository.saveAll(planToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${planToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}