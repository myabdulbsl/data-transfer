package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.ExportModel
import au.com.blueskytel.datatransfer.model.common.InvoiceModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.ExportFromRepository
import au.com.blueskytel.datatransfer.repository.from.InvoiceFromRepository
import au.com.blueskytel.datatransfer.repository.to.ExportToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.repository.to.InvoiceToRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class InvoiceService : IntegrationService {

    override var integrationId: Int = 10
    override var integrationType = 0
    override var integrationName = "Invoice"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var invoiceFromRepository: InvoiceFromRepository
    lateinit var invoiceToRepository: InvoiceToRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<InvoiceModel> = listOf()
        try {
            val invoiceTo = if (invoiceToRepository.count() > 0)
                invoiceToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (invoiceTo != null)
                invoiceTo.id?.let { invoiceFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<InvoiceModel>
            else
                invoiceFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val invoiceToList = data as List<InvoiceModel>
            if (invoiceToList != null && invoiceToList.isNotEmpty()) {
                invoiceToRepository.saveAll(invoiceToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${invoiceToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}