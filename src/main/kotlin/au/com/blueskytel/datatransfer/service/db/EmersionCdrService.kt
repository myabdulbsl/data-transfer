package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.EmersionCdrModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.EmersionCdrFromRepository
import au.com.blueskytel.datatransfer.repository.to.EmersionCdrToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class EmersionCdrService : IntegrationService {

    override var integrationId: Int = 8
    override var integrationType = 0
    override var integrationName = "EmersionCdr"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var emersionCdrToRepository: EmersionCdrToRepository
    lateinit var emersionCdrFromRepository: EmersionCdrFromRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<EmersionCdrModel> = listOf()
        try {
            val emersionCdrTo = if (emersionCdrToRepository.count() > 0)
                emersionCdrToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (emersionCdrTo != null)
                emersionCdrTo.id?.let { emersionCdrFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<EmersionCdrModel>
            else
                emersionCdrFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val emersionCdrToList = data as List<EmersionCdrModel>
            if (emersionCdrToList != null && emersionCdrToList.isNotEmpty()) {
                emersionCdrToRepository.saveAll(emersionCdrToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${emersionCdrToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}