package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.IddPrefixModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.IddPrefixFromRepository
import au.com.blueskytel.datatransfer.repository.to.IddPrefixToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class IddPrefixService : IntegrationService {

    override var integrationId: Int = 9
    override var integrationType = 0
    override var integrationName = "IddPrefix"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var iddPrefixFromRepository: IddPrefixFromRepository
    lateinit var iddPrefixToRepository: IddPrefixToRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<IddPrefixModel> = listOf()
        try {
            val iddPrefixTo = if (iddPrefixToRepository.count() > 0)
                iddPrefixToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (iddPrefixTo != null)
                iddPrefixTo.id?.let { iddPrefixFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<IddPrefixModel>
            else
                iddPrefixFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val iddPrefixToList = data as List<IddPrefixModel>
            if (iddPrefixToList != null && iddPrefixToList.isNotEmpty()) {
                iddPrefixToRepository.saveAll(iddPrefixToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${iddPrefixToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}