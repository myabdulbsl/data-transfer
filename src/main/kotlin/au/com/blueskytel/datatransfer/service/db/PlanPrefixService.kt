package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.PlanPrefixModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.PlanPrefixFromRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.repository.to.PlanPrefixToRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class PlanPrefixService : IntegrationService {

    override var integrationId: Int = 13
    override var integrationType = 0
    override var integrationName = "PlanPrefix"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var planPrefixFromRepository: PlanPrefixFromRepository
    lateinit var planPrefixToRepository: PlanPrefixToRepository

    override fun getSource(): Any? {

        var planPrefixFromList: List<PlanPrefixModel> = listOf()
        try {
            planPrefixFromList = planPrefixFromRepository.findAll()

            if (planPrefixFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${planPrefixFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return planPrefixFromList
    }

    override fun getHandler(data: Any) {

        try {
            val planPrefixToList = data as List<PlanPrefixModel>
            if (planPrefixToList != null && planPrefixToList.isNotEmpty()) {
                planPrefixToRepository.deleteAll()
                planPrefixToRepository.saveAll(planPrefixToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${planPrefixToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}