package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.CustomerInvoiceModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.CustomerInvoiceFromRepository
import au.com.blueskytel.datatransfer.repository.to.CustomerInvoiceToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class CustomerInvoiceService : IntegrationService {

    override var integrationId: Int = 11
    override var integrationType = 0
    override var integrationName = "CustomerInvoice"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var customerInvoiceFromRepository: CustomerInvoiceFromRepository
    lateinit var customerInvoiceToRepository: CustomerInvoiceToRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<CustomerInvoiceModel> = listOf()
        try {
            val customerInvoiceTo = if (customerInvoiceToRepository.count() > 0)
                customerInvoiceToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (customerInvoiceTo != null)
                customerInvoiceTo.id?.let { customerInvoiceFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<CustomerInvoiceModel>
            else
                customerInvoiceFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val customerInvoiceToList = data as List<CustomerInvoiceModel>
            if (customerInvoiceToList != null && customerInvoiceToList.isNotEmpty()) {
                customerInvoiceToRepository.saveAll(customerInvoiceToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${customerInvoiceToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}