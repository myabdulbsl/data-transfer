package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.CustomerModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.CustomerFromRepository
import au.com.blueskytel.datatransfer.repository.to.CustomerToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class CustomerService : IntegrationService {

    override var integrationId: Int = 6
    override var integrationType = 0
    override var integrationName = "Customer"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var customerFromRepository: CustomerFromRepository
    lateinit var customerToRepository: CustomerToRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<CustomerModel> = listOf()
        try {
            val customerTo = if (customerToRepository.count() > 0)
                customerToRepository.findTop1ByOrderByIdDesc()
            else
                null
            callTypeFromList = if (customerTo != null)
                customerTo.id?.let { customerFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<CustomerModel>
            else
                customerFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val customerToList = data as List<CustomerModel>
            if (customerToList != null && customerToList.isNotEmpty()) {
                customerToRepository.saveAll(customerToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${customerToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}