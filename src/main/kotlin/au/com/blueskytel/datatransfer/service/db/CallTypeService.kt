package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.CallTypeModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.CallTypeFromRepository
import au.com.blueskytel.datatransfer.repository.to.CallTypeToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class CallTypeService : IntegrationService {

    override var integrationId: Int = 3
    override var integrationType = 0
    override var integrationName = "CallType"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var callTypeFromRepository: CallTypeFromRepository
    lateinit var callTypeToRepository: CallTypeToRepository

    override fun getSource(): Any? {

        var callTypeFromList: List<CallTypeModel> = listOf()
        try {
            val callTypeTo = if (callTypeToRepository.count() > 0)
                callTypeToRepository.findTop1ByOrderByCallTypeIDDesc()
            else
                null
            callTypeFromList = if (callTypeTo != null)
                callTypeTo.callTypeID?.let { callTypeFromRepository.findTop1000ByCallTypeIDGreaterThanOrderByCallTypeID(it) } as List<CallTypeModel>
            else
                callTypeFromRepository.findTop1000ByCallTypeIDGreaterThanOrderByCallTypeID(0)

            if (callTypeFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${callTypeFromList.size} rows from CDR DB"))

        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }
        return callTypeFromList
    }

    override fun getHandler(data: Any) {

        try {
            val callTypeToList = data as List<CallTypeModel>
            if (callTypeToList != null && callTypeToList.isNotEmpty()) {
                callTypeToRepository.saveAll(callTypeToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${callTypeToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}