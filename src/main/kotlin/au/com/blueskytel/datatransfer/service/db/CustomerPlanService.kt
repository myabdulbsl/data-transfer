package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.CustomerPlanModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.CustomerPlanFromRepository
import au.com.blueskytel.datatransfer.repository.to.CustomerPlanToRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class CustomerPlanService : IntegrationService {

    override var integrationId: Int = 7
    override var integrationType = 0
    override var integrationName = "CustomerPlan"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var customerPlanFromRepository: CustomerPlanFromRepository
    lateinit var customerPlanToRepository: CustomerPlanToRepository

    override fun getSource(): Any? {

        var customerPlanFromList: List<CustomerPlanModel> = listOf()
        try {
            val customerPlanTo = if (customerPlanToRepository.count() > 0)
                customerPlanToRepository.findTop1ByOrderByIdDesc()
            else
                null
            customerPlanFromList = if (customerPlanTo != null)
                customerPlanTo.id?.let { customerPlanFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<CustomerPlanModel>
            else
                customerPlanFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (customerPlanFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${customerPlanFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return customerPlanFromList
    }

    override fun getHandler(data: Any) {

        try {
            val customerPlanToList = data as List<CustomerPlanModel>
            if (customerPlanToList != null && customerPlanToList.isNotEmpty()) {
                customerPlanToRepository.saveAll(customerPlanToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${customerPlanToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}