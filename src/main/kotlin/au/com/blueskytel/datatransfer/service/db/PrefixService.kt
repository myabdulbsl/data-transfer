package au.com.blueskytel.datatransfer.service.db

import au.com.blueskytel.datatransfer.model.common.PlanModel
import au.com.blueskytel.datatransfer.model.common.PrefixModel
import au.com.blueskytel.datatransfer.model.to.IntegrationLogModel
import au.com.blueskytel.datatransfer.repository.from.PrefixFromRepository
import au.com.blueskytel.datatransfer.repository.to.IntegrationLogRepository
import au.com.blueskytel.datatransfer.repository.to.PrefixToRepository
import au.com.blueskytel.datatransfer.service.IntegrationService
import org.springframework.stereotype.Service

@Service
class PrefixService : IntegrationService {

    override var integrationId: Int = 14
    override var integrationType = 0
    override var integrationName = "Prefix"
    override lateinit var integrationLogRepository: IntegrationLogRepository
    lateinit var prefixToRepository: PrefixToRepository
    lateinit var prefixFromRepository: PrefixFromRepository

    override fun getSource(): Any? {

        var prefixFromList: List<PrefixModel> = listOf()
        try {
            val prefixTo = if (prefixToRepository.count() > 0)
                prefixToRepository.findTop1ByOrderByIdDesc()
            else
                null
            prefixFromList = if (prefixTo != null)
                prefixTo.id?.let { prefixFromRepository.findTop1000ByIdGreaterThanOrderById(it) } as List<PrefixModel>
            else
                prefixFromRepository.findTop1000ByIdGreaterThanOrderById(0)

            if (prefixFromList.isNotEmpty())
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Retrieved ${prefixFromList.size} rows from CDR DB"))
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "CDR DB: ${e.message}", isError = true))
        }

        return prefixFromList
    }

    override fun getHandler(data: Any) {
        try {
            val prefixToList = data as List<PrefixModel>
            if (prefixToList != null && prefixToList.isNotEmpty()) {
                prefixToRepository.saveAll(prefixToList)
                integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Pushed ${prefixToList.size} rows to Local DB"))
            }
        } catch (e: Exception) {
            integrationLogRepository.save(IntegrationLogModel(integrationId = integrationId, integrationName = integrationName, message = "Local DB: ${e.message}", isError = true))
        }

    }
}