package au.com.blueskytel.datatransfer.model.to

import lombok.Data
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "file_dump")
@Data
class FileDumpModel(
        @Id
        var integrationId: Long? = null,

        @Column(nullable = true)
        var integrationName: String?,

        @Column(nullable = true)
        var currentTableId: Long?,

        @Column(nullable = true)
        val createdAt: LocalDateTime? = LocalDateTime.now(),

        @Column(nullable = true)
        var updatedAt: LocalDateTime? = LocalDateTime.now()
)