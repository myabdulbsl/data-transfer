package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import java.io.Serializable
import javax.persistence.*


@Entity
@Table(name = "Plans2Prefixes")
@IdClass(PlanPrefixId::class)
@Data
class PlanPrefixModel(
        @Id @Column(name = "PlanID") var planId: Int? = null,
        @Id @Column(name = "PrefixID") var prefixId: Int? = null,
        @Column(name = "CallTypeID") var callTypeId: Int? = null,
        @Column(name = "SellRate") var sellRate: Double? = null,
        @Column(name = "perDuration") var perDuration: String? = null
)

class PlanPrefixId(val planId: Int? = null, private val prefixId: Int? = null) : Serializable