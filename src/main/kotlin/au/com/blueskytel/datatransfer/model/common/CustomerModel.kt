package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Customers")
@Data
class CustomerModel(
        @Id @Column(name = "CustomerID") var id: Long? = null,
        @Column(name = "JohlanDB_ID") var johlanDbId: Int? = null,
        @Column(name = "CustomerName") var customerName: String? = null
)