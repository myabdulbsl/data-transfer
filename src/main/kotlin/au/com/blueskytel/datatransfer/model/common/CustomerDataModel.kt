package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Customer_CDR_Data")
@Data
class CustomerDataModel(
        @Id @Column(name = "CDR_ID") var id: Long? = null,
        @Column(name = "asterisk_cdr_ID") var asteriskCdrId: String? = null,
        @Column(name = "CustomerPlanID") var customerPlanID: String? = null,
        @Column(name = "CallType_ID") var callTypeId: String? = null,
        @Column(name = "SellRate") var sellRate: String? = null,
        @Column(name = "perDuration") var perDuration: String? = null,
        @Column(name = "IncomingCharged") var incomingCharged: String? = null,
        @Column(name = "CallDirection") var callDirection: String? = null,
        @Column(name = "CallTotal") var callTotal: String? = null,
        @Column(name = "BilledTime") var billedTime: String? = null
)