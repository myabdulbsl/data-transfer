package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "CDR_Exports")
@Data
class ExportModel(
        @Id @Column(name = "CDR_Export_ID") var id: Long? = null,
        @Column(name = "CustomerID") var customerId: String? = null,
        @Column(name = "ExportDir") var exportDir: String? = null,
        @Column(name = "Accountcodes") var accountCodes: String? = null
)