package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Invoices2Customer_CDR_Data")
@Data
class CustomerInvoiceModel(
        @Id @Column(name = "InvoiceID") var id: Long? = null,
        @Column(name = "CDR_ID") var cdrId: Int? = null
)