package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "Invoices")
@Data
class InvoiceModel(
        @Id @Column(name = "InvoiceID") var id: Long? = null,
        @Column(name = "InvoiceDate") var invoiceDate: LocalDateTime? = null,
        @Column(name = "CustomerPlanID") var customerPlanID: Int? = null
)