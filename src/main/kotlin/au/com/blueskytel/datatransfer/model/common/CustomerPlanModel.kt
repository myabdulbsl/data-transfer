package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Customers2Plans")
@Data
class CustomerPlanModel(
        @Id @Column(name = "CustomerPlanID") var id: Long? = null,
        @Column(name = "CustomerID") var customerId: Int? = null,
        @Column(name = "PlanID") var planId: Int? = null,
        @Column(name = "accountcode") var accountCode: String? = null,
        @Column(name = "IncomingPlanID") var incomingPlanId: Int? = null,
        @Column(name = "Channels") var channels: Int? = null
)