package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "CallTypes")
@Data
class CallTypeModel(
        @Id @Column(name = "CallTypeID") var callTypeID: Long? = null,
        @Column(name = "CallType") var callType: String? = null
)