package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Prefixes")
@Data
class PrefixModel(
        @Id @Column(name = "PrefixID") var id: Long? = null,
        @Column(name = "Prefix") var prefix: String? = null,
        @Column(name = "NumberLength") var numberLength: Int? = null,
        @Column(name = "BuyRate") var buyRate: Double? = null
)