package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Emersion_CDRs")
@Data
class EmersionCdrModel(
        @Id @Column(name = "Emersion_CDR_ID") var id: Long? = null,
        @Column(name = "CDR_ID") var cdrId: Int? = null
)