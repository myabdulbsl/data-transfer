package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "asterisk_cdr_deleted")
@Data
class AsteriskDeletedModel(
        @Id @Column(name = "id") var id: Long? = null,
        @Column(name = "calldate") var callDate: LocalDateTime? = null,
        @Column(name = "clid") var clId: String? = null,
        @Column(name = "src") var src: String? = null,
        @Column(name = "dst") var dst: String? = null,
        @Column(name = "diallingnumber") var diallingNumber: String? = null,
        @Column(name = "diallednumber") var dialledNumber: String? = null,
        @Column(name = "dcontext") var dconText: String? = null,
        @Column(name = "channel") var channel: String? = null,
        @Column(name = "dstchannel") var dstChannel: String? = null,
        @Column(name = "lastapp") var lastApp: String? = null,
        @Column(name = "lastdata") var lastData: String? = null,
        @Column(name = "duration") var duration: Int? = null,
        @Column(name = "billsec") var billsec: Int? = null,
        @Column(name = "disposition") var disposition: String? = null,
        @Column(name = "amaflags") var amaFlags: Int? = null,
        @Column(name = "accountcode") var accountCode: String? = null,
        @Column(name = "uniqueid") var uniqueId: String? = null,
        @Column(name = "userfield") var userField: String? = null,
        @Column(name = "duplicate") var duplicate: String? = null,
        @Column(name = "duplicatetoaccountcode") var duplicateToAccountCode: String? = null,
        @Column(name = "calldirection") var callDirection: String? = null,
        @Column(name = "callforwardnumber") var callForwardNumber: String? = null,
        @Column(name = "incomingcharged") var incomingCharged: String? = null
)