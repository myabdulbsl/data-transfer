package au.com.blueskytel.datatransfer.model.to

import lombok.Data
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "integration_log")
@Data
class IntegrationLogModel(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private var id: Int? = null,

        @Column(nullable = true)
        private var integrationId: Int?,

        @Column(nullable = true)
        private var integrationName: String?,

        @Column(length = 10000000, nullable = true)
        private var message: String?,

        @Column(nullable = false)
        private var isError: Boolean = false,

        @Column(nullable = true)
        private val createdAt: LocalDateTime? = LocalDateTime.now(),

        @Column(nullable = true)
        private val updatedAt: LocalDateTime? = LocalDateTime.now()
)