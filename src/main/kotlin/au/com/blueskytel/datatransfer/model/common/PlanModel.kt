package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "Plans")
@Data
class PlanModel(
        @Id @Column(name = "PlanID") var id: Long? = null,
        @Column(name = "PlanName") var planName: String? = null,
        @Column(name = "Package") var packageName: Int? = null,
        @Column(name = "IncludedCredit") var includedCredit: Double? = null,
        @Column(name = "IncludedCallCountPerChannel") var includedCallCountPerChannel: Int? = null
)