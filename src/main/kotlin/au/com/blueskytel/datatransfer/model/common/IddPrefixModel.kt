package au.com.blueskytel.datatransfer.model.common

import lombok.Data
import javax.persistence.*

@Entity
@Table(name = "IDDPrefixes")
@Data
class IddPrefixModel(
        @Id @Column(name = "IDD_ID") var id: Long? = null,
        @Column(name = "Country") var country: String? = null,
        @Column(name = "Prefix") var prefix: String? = null,
        @Column(name = "BuyRate") var buyRate: Double? = null,
        @Column(name = "SellRate") var sellRate: Double? = null
)